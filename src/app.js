import './assets/scss/styles.scss';

const container = document.querySelector('section')
window.addEventListener('resize', resizeSection, false)


const backgroundCanvas = document.querySelector('#background');
const tubesCanvas = document.querySelector('#tubes');
const tubesOverlayCanvas = document.querySelector('#tubes-overlay');
const overlayCanvas = document.querySelector('#overlay');
const soloTubeCanvas = document.querySelector('#solo-tube');
let context;
// const canvas = document.querySelector('#background');
// const canvas = document.querySelector('#background');
// const context = canvas.getContext('2d');
function resizeSection() {
    container.style.height = backgroundCanvas.offsetHeight + 'px';
}
const topTriangleColor = '#242C47'
function drawIn(canvas) {
    context = canvas.getContext('2d')
}

const tubes = [
    {
        x: 100,
        y: 170,
        width: 300,
        height: 42,
        gradientStops: [[0, "#E5B1FA"], [1, "#B6B3FC"]],
    },
    {
        x: 160,
        y: 320,
        width: 900,
        height: 42,
        gradientStops: [[0, "#5CA6F1"], [0.7, "#5CA6F1"], [1, "#C7F578"]]
    },
    {
        x: 260,
        y: 360,
        width: 850,
        height: 42,
        gradientStops: [[0, "#9FC3F4"], [1, "#9FC3F4"]]
    },
    {
        x: 450,
        y: 508,
        width: 800,
        height: 98,
        gradientStops: [[0, "#7EEEC4"], [1, "#7EEEC4"]]
    },
    {
        x: 480,
        y: 530,
        width: 356,
        height: 42,
        gradientStops: [[0, "#7EEEC4"], [1, "#7EEEC4"]]
    },
    {
        x: 340,
        y: 520,
        width: 1200,
        height: 42,
        gradientStops: [[0, "#5CA6F1"], [0.2, "#5CA6F1"], [1, "#F2B1A3"]]
    },
]


function drawTube(tube) {
    let { x, y, width, height, gradientStops } = tube
    const cornerRadius = height
    context.beginPath();


    // Set faux rounded corners
    context.lineJoin = "round";
    context.lineWidth = cornerRadius
    const gradient = context.createLinearGradient(x, y + height, x + width, y);
    gradientStops.forEach(stop => gradient.addColorStop(stop[0], stop[1]))
    context.strokeStyle = gradient;
    context.save()
    const angle = -30;

    context.translate(x + width / -2, y + height / -2);
    context.rotate((Math.PI / 180) * angle);


    context.strokeRect(x + (cornerRadius / 2), y + (cornerRadius / 2), width - cornerRadius, height - cornerRadius);
    context.restore();

    context.closePath();
}

function drawDarkRectangle() {
    context.beginPath();
    context.strokeStyle = '#000000';
    context.moveTo(0, 790);
    context.lineTo(1424, 0);
    context.stroke();
    context.lineTo(0, 0);
    context.lineTo(0, 790);
    context.fillStyle = topTriangleColor;
    context.fill();
    context.closePath();
}


drawIn(tubesCanvas)
drawTubes(tubes);
drawIn(backgroundCanvas);
drawDarkRectangle();
drawCircle(500, 160, 32, [[0, '#D6D6D6']]);
drawCircle(780, 800, 32, [[0, '#84C2C9'], [0.8, '#66ACEC'], [1, '#66ACEC']]);
drawTriangle(1040, 190, 80, '#5FA1F5');
drawIn(tubesOverlayCanvas)
drawCircle(240, 710, 60, [[0, "#EC9491"], [1, "#BE253F"]], {
    color: 'rgba(0, 0, 0, 0.2)',
    offsetX: -20,
    offsetY: 40,
    blur: 70
})
drawTube({
    x: 20,
    y: 380,
    width: 526,
    height: 98,
    gradientStops: [[0, "#70D7E7"], [1, "#70D7E7"]]
})
drawIn(soloTubeCanvas)
drawTube({
    x: 290,
    y: 500,
    width: 400,
    height: 42,
    gradientStops: [[0, "#E5B1FA"], [1, "#B6B3FC"]],
})
drawIn(overlayCanvas)
drawTriangle(190, 370, 80, '#7EEEC4')
drawCircle(300, 280, 20, [[0, "#ED58C6"], [1, "#B52154"]], {
    color: "#666",
    offsetX: 20,
    offsetY: 20,
    blur: 70
})

resizeSection()

function drawTriangle(x, y, length, color) {
    context.beginPath();
    context.moveTo(x, y);
    context.lineTo(x + length * Math.cos((Math.PI / 180) * 60), y - length * Math.sin((Math.PI / 180) * 60));
    context.lineTo(x + length, y);
    context.lineTo(x, y);
    context.fillStyle = color;
    context.fill();
    context.closePath();
}

function drawCircle(x, y, r, color, shadow) {
    context.beginPath();
    const gradient = context.createLinearGradient(x - r / 2, y - r, x + r / 2, y + r);
    color.forEach(c => gradient.addColorStop(c[0], c[1]))
    context.arc(x, y, r, 0, 2 * Math.PI);
    context.fillStyle = gradient;
    context.save()
    if (shadow) {
        setShadow(context, shadow.color, shadow.offsetX, shadow.offsetY, shadow.blur);
    }
    context.fill();
    context.closePath();
    context.restore();
}

function drawTubes(tubes) {
    tubes.forEach(drawTube);
}

function setShadow(ctx, color, ox, oy, blur) {
    ctx.shadowColor = color;
    ctx.shadowOffsetX = ox;
    ctx.shadowOffsetY = oy;
    ctx.shadowBlur = blur;
}